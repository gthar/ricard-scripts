#!/usr/bin/env python

"""example that creates a report, runs it, downloads the result and then
deletes it

dimensions are documented here:
https://developers.google.com/doubleclick-advertisers/v3.4/dimensions
"""

import time
import io

import logging

import httplib2
import googleapiclient
from googleapiclient import discovery
from oauth2client.service_account import (
    ServiceAccountCredentials,
)

API_NAME = "dfareporting"
API_VERSION = "v3.4"
CHUNK_SIZE = 32 * 1024 * 1024
OAUTH_SCOPES = [
    "https://www.googleapis.com/auth/dfareporting",
    "https://www.googleapis.com/auth/dfatrafficking",
    "https://www.googleapis.com/auth/ddmconversions",
]


PROFILE_ID = 6488399
ACCOUNT_CREDS = "service-account-credentials.json"

# note: this structure is specific to `STANDARD` report type
REPORT_NAME = "test"
DATE_RANGE = {
    "startDate": "2021-03-03",
    "endDate": "2021-04-05",
}
DIMENSIONS = [
    {"name": "dfa:date"},
    {"name": "dfa:advertiserId"},
    {"name": "dfa:advertiser"},
    {"name": "dfa:campaignId"},
    {"name": "dfa:campaign"},
    {"name": "dfa:siteId"},
    {"name": "dfa:site"},
    {"name": "dfa:placementId"},
    {"name": "dfa:placement"},
    {"name": "dfa:creativeId"},
    {"name": "dfa:creativeType"},
    {"name": "dfa:creativeSize"},
    {"name": "dfa:placementRate"},
    {"name": "dfa:placementCostStructure"},
    {"name": "dfa:packageRoadblockTotalBookedUnits"},
    {"name": "dfa:placementTotalBookedUnits"},
]
METRIC_NAMES = [
    "dfa:impressions",
    "dfa:clicks",
    "dfa:clickRate",
    "dfa:activeViewViewableImpressions",
    "dfa:activeViewMeasurableImpressions",
    "dfa:richMediaVideoPlays",
    "dfa:richMediaVideoCompletions",
    "dfa:richMediaAverageVideoViewTime",
    "dfa:activeViewAverageViewableTimeSecond",
    "dfa:plannedMediaCost",
    "dfa:mediaCost",
    "dfa:dbmCostUsd",
    "dfa:dbmCost",
    "dfa:effectiveCpm",
]

DIMENSION_FILTERS = [
    {"dimensionName": "dfa:advertiserId", "value": "8628535"},
    {"dimensionName": "dfa:advertiserId", "value": "8626735"},
    {"dimensionName": "dfa:advertiserId", "value": "10304642"},
    {"dimensionName": "dfa:advertiserId", "value": "10265304"},
    {"dimensionName": "dfa:advertiserId", "value": "10359330"},
    {"dimensionName": "dfa:advertiserId", "value": "10357830"},
    {"dimensionName": "dfa:advertiserId", "value": "8501474"},
    {"dimensionName": "dfa:advertiserId", "value": "8521918"},
    {"dimensionName": "dfa:advertiserId", "value": "6917428"},
    {"dimensionName": "dfa:advertiserId", "value": "6921230"},
    {"dimensionName": "dfa:advertiserId", "value": "6921231"},
    {"dimensionName": "dfa:advertiserId", "value": "6916423"},
    {"dimensionName": "dfa:advertiserId", "value": "9506051"},
    {"dimensionName": "dfa:advertiserId", "value": "9506911"},
    {"dimensionName": "dfa:advertiserId", "value": "6929827"},
    {"dimensionName": "dfa:advertiserId", "value": "6927217"},
    {"dimensionName": "dfa:advertiserId", "value": "6923031"},
    {"dimensionName": "dfa:advertiserId", "value": "6916644"},
    {"dimensionName": "dfa:advertiserId", "value": "6927639"},
    {"dimensionName": "dfa:advertiserId", "value": "6929650"},
    {"dimensionName": "dfa:advertiserId", "value": "9082201"},
    {"dimensionName": "dfa:advertiserId", "value": "9052437"},
    {"dimensionName": "dfa:advertiserId", "value": "8815496"},
    {"dimensionName": "dfa:advertiserId", "value": "8826630"},
    {"dimensionName": "dfa:advertiserId", "value": "9081313"},
    {"dimensionName": "dfa:advertiserId", "value": "9081898"},
    {"dimensionName": "dfa:advertiserId", "value": "8160147"},
    {"dimensionName": "dfa:advertiserId", "value": "8160251"},
    {"dimensionName": "dfa:advertiserId", "value": "8626753"},
    {"dimensionName": "dfa:advertiserId", "value": "8619657"},
    {"dimensionName": "dfa:advertiserId", "value": "8168332"},
    {"dimensionName": "dfa:advertiserId", "value": "8168038"},
    {"dimensionName": "dfa:advertiserId", "value": "8907945"},
    {"dimensionName": "dfa:advertiserId", "value": "8908560"},
    {"dimensionName": "dfa:advertiserId", "value": "10632390"},
    {"dimensionName": "dfa:advertiserId", "value": "10656724"},
]


REPORT_REQUEST_BODY = {
    "type": "STANDARD",
    "name": REPORT_NAME,
    "criteria": {
        "dimensions": DIMENSIONS,
        "dateRange": DATE_RANGE,
        "metricNames": METRIC_NAMES,
        "dimensionFilters": DIMENSION_FILTERS,
    },
}


logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def mk_service(creds, scopes, api_name, api_version):
    """Create an authenticate API service"""
    credentials = ServiceAccountCredentials.from_json_keyfile_name(creds, scopes)
    http = credentials.authorize(httplib2.Http())
    return discovery.build(api_name, api_version, http=http)


def create_report(service, profile_id, request_body):
    """Create a report and return its ID"""
    logger.info("> creating report")
    request = service.reports().insert(profileId=profile_id, body=request_body)
    report_id = request.execute()["id"]
    logger.info("    report id: %s", report_id)
    return report_id


def check_status(service, report_id, file_id):
    """Check the status of a running request"""
    request = service.files().get(reportId=report_id, fileId=file_id)
    response = request.execute()
    return response["status"]


def run_report(service, report_id):
    """Run a report, wait until the report is available and return the
    resulting file ID"""
    logger.info("> running report `%s`", report_id)
    request = service.reports().run(profileId=PROFILE_ID, reportId=report_id)
    response = request.execute()
    file_id = response["id"]

    status = check_status(service, report_id, file_id)
    while status != "REPORT_AVAILABLE":
        logger.info("    waiting for report to run...")
        time.sleep(1)
        status = check_status(service, report_id, file_id)

    logger.info("    report available; file id: %s", file_id)
    return file_id


def mk_fname(report_file, out_dir="."):
    """Create the output file name"""
    fname = report_file.get("fileName") or report_file["id"]
    if report_file["format"] == "CSV":
        extension = "csv"
    else:
        extension = "xml"
    return f"{out_dir}/{fname}.{extension}"


def dl_report(service, report_id, file_id):
    """Download a report and return the location where it's been saved"""
    logger.info("> downloading report")
    report_file = service.files().get(reportId=report_id, fileId=file_id).execute()

    fname = mk_fname(report_file)
    out_file = io.FileIO(fname, mode="wb")

    request = service.files().get_media(reportId=report_id, fileId=file_id)
    downloader = googleapiclient.http.MediaIoBaseDownload(
        out_file, request, chunksize=CHUNK_SIZE
    )

    while True:
        logger.info("    downloading a chunk...")
        _, finished = downloader.next_chunk()
        if finished:
            break

    logger.info("    download finished, saved at: `%s`", fname)

    return fname


def rm_report(service, profile_id, report_id):
    """Delete an existing report"""
    logger.info("> deleting report `%s`", report_id)
    request = service.reports().delete(profileId=profile_id, reportId=report_id)
    request.execute()
    logger.info("    deleted report `%s`", report_id)


def ls_reports(service, profile_id):
    """List the reports"""
    request = service.reports().list(profileId=profile_id)
    while True:
        response = request.execute()
        for report in response["items"]:
            yield report["id"]
        if response["items"] and response["nextPageToken"]:
            request = service.reports().list_next(request, response)
        else:
            break


def main():
    """do the thing"""
    service = mk_service(ACCOUNT_CREDS, OAUTH_SCOPES, API_NAME, API_VERSION)
    report_id = create_report(service, PROFILE_ID, REPORT_REQUEST_BODY)
    try:
        file_id = run_report(service, report_id)
        dl_report(service, report_id, file_id)
    finally:
        rm_report(service, PROFILE_ID, report_id)


if __name__ == "__main__":
    main()
